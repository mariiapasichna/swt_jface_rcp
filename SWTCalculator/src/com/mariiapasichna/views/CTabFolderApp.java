package com.mariiapasichna.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

/**
 * Instances of this class implement the notebook user interface metaphor. It
 * allows the user to select a notebook page from set of pages. The
 * {@code CTabFolderApp} class extends CTabFolder.
 * 
 * @author Mariia Pasichna
 *
 */
public class CTabFolderApp extends CTabFolder {
	private CTabItemCalculator panelCalculator;
	private CTabItemHistory panelHistory;

	/**
	 * Constructs a new instance of this class given its parent and a style value
	 * describing its behavior and appearance.
	 * 
	 * @param parent  a widget which will be the parent of the new instance (cannot
	 *                be null)
	 * @param display instance responsible for managing the connection between SWT
	 *                and the underlying operating system.
	 */
	public CTabFolderApp(Composite parent, Display display) {
		super(parent, SWT.NONE);
		setSelectionForeground(display.getSystemColor(SWT.COLOR_RED));
		setLayout(new FillLayout());
		panelCalculator = new CTabItemCalculator(this);
		panelHistory = new CTabItemHistory(this);
	}

	/**
	 * Return the value stored in field panelCalculator.
	 * 
	 * @return panelCalculator the CTabItemCalculator value stored in variable
	 *         panelCalculator
	 */
	public CTabItemCalculator getPanelCalculator() {
		return panelCalculator;
	}

	/**
	 * Set the value into field panelCalculator.
	 * 
	 * @param panelCalculator the CTabItemCalculator value to be stored in variable
	 *                        panelCalculator
	 */
	public void setPanelCalculator(CTabItemCalculator panelCalculator) {
		this.panelCalculator = panelCalculator;
	}

	/**
	 * Return the value stored in field panelHistory.
	 * 
	 * @return panelHistory the CTabItemHistory value stored in variable
	 *         panelHistory
	 */
	public CTabItemHistory getPanelHistory() {
		return panelHistory;
	}

	/**
	 * Set the value into field panelHistory.
	 * 
	 * @param panelHistory the CTabItemHistory value to be stored in variable
	 *                     panelHistory
	 */
	public void setPanelHistory(CTabItemHistory panelHistory) {
		this.panelHistory = panelHistory;
	}
}
package com.mariiapasichna.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.List;

/**
 * Instances of this class represent a selectable user interface object that
 * represent a page in a notebook widget. The {@code CTabItemHistory} class
 * extends CTabItem.
 * 
 * @author Mariia Pasichna
 *
 */
public class CTabItemHistory extends CTabItem {
	private List results;

	/**
	 * Constructs a new instance of this class given its parent (which must be a
	 * CTabFolder) and describe its behavior and appearance.
	 * 
	 * @param parent a CTabFolder which will be the parent of the new instance
	 *               (cannot be null)
	 */
	public CTabItemHistory(CTabFolder parent) {
		super(parent, SWT.NONE);
		setText("History");

		results = new List(parent, SWT.BORDER | SWT.V_SCROLL);
		setControl(results);
	}

	/**
	 * Return the value stored in field results.
	 * 
	 * @return results the List value stored in variable results
	 */
	public List getResults() {
		return results;
	}

	/**
	 * Set the value into field results.
	 * 
	 * @param results the List value to be stored in variable results
	 */
	public void setResults(List results) {
		this.results = results;
	}
}
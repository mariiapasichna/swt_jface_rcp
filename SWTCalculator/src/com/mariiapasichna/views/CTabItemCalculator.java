package com.mariiapasichna.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Instances of this class represent a selectable user interface object that
 * represent a page in a notebook widget. The {@code CTabItemCalculator} class
 * extends CTabItem.
 * 
 * @author Mariia Pasichna
 *
 */
public class CTabItemCalculator extends CTabItem {
	private Text textFieldInput1;
	private Text textFieldInput2;
	private Text textFieldResult;
	private Button checkBox;
	private Button buttonCalculate;
	private Label labelResult;
	private Combo comboOperations;

	/**
	 * Constructs a new instance of this class given its parent (which must be a
	 * CTabFolder) and describe its behavior and appearance.
	 * 
	 * @param parent a CTabFolder which will be the parent of the new instance
	 *               (cannot be null)
	 */
	public CTabItemCalculator(CTabFolder parent) {
		super(parent, SWT.NONE);
		setText("Calculator");

		Composite composite = new Composite(parent, SWT.BORDER);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(new GridLayout(3, false));

		Composite innerComposite = new Composite(composite, SWT.NONE);
		innerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		innerComposite.setLayout(new GridLayout(3, true));

		textFieldInput1 = new Text(innerComposite, SWT.BORDER);
		textFieldInput1.setText("0.0");
		textFieldInput1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

		comboOperations = new Combo(innerComposite, SWT.READ_ONLY);
		comboOperations.setItems("+", "-", "/", "*");
		comboOperations.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));

		textFieldInput2 = new Text(innerComposite, SWT.BORDER);
		textFieldInput2.setText("0.0");
		textFieldInput2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));

		checkBox = new Button(composite, SWT.CHECK);
		checkBox.setText("Calculate on the fly");
		checkBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 2, 1));

		buttonCalculate = new Button(composite, SWT.PUSH);
		buttonCalculate.setText("Calculate");
		buttonCalculate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));

		labelResult = new Label(composite, SWT.NONE);
		labelResult.setText("Result:");
		labelResult.setLayoutData(new GridData(SWT.LEFT, SWT.END, true, true, 1, 1));

		textFieldResult = new Text(composite, SWT.BORDER);
		textFieldResult.setText("0.0");
		textFieldResult.setLayoutData(new GridData(SWT.FILL, SWT.END, true, true, 2, 1));

		setControl(composite);
	}

	/**
	 * Return the value stored in field textFieldInput1.
	 * 
	 * @return textFieldInput1 the Text value stored in variable textFieldInput1
	 */
	public Text getTextFieldInput1() {
		return textFieldInput1;
	}

	/**
	 * Set the value into field textFieldInput1.
	 * 
	 * @param textFieldInput1 the Text value to be stored in variable
	 *                        textFieldInput1
	 */
	public void setTextFieldInput1(Text textFieldInput1) {
		this.textFieldInput1 = textFieldInput1;
	}

	/**
	 * Return the value stored in field textFieldInput2.
	 * 
	 * @return textFieldInput2 the Text value stored in variable textFieldInput2
	 */
	public Text getTextFieldInput2() {
		return textFieldInput2;
	}

	/**
	 * Set the value into field textFieldInput2.
	 * 
	 * @param textFieldInput2 the Text value to be stored in variable
	 *                        textFieldInput2
	 */
	public void setTextFieldInput2(Text textFieldInput2) {
		this.textFieldInput2 = textFieldInput2;
	}

	/**
	 * Return the value stored in field textFieldResult.
	 * 
	 * @return textFieldResult the Text value stored in variable textFieldResult
	 */
	public Text getTextFieldResult() {
		return textFieldResult;
	}

	/**
	 * Set the value into field textFieldResult.
	 * 
	 * @param textFieldResult the Text value to be stored in variable
	 *                        textFieldResult
	 */
	public void setTextFieldResult(Text textFieldResult) {
		this.textFieldResult = textFieldResult;
	}

	/**
	 * Return the value stored in field checkBox.
	 * 
	 * @return checkBox the Button value stored in variable checkBox
	 */
	public Button getCheckBox() {
		return checkBox;
	}

	/**
	 * Set the value into field checkBox.
	 * 
	 * @param checkBox the Button value to be stored in variable checkBox
	 */
	public void setCheckBox(Button checkBox) {
		this.checkBox = checkBox;
	}

	/**
	 * Return the value stored in field buttonCalculate.
	 * 
	 * @return buttonCalculate the Button value stored in variable buttonCalculate
	 */
	public Button getButtonCalculate() {
		return buttonCalculate;
	}

	/**
	 * Set the value into field buttonCalculate.
	 * 
	 * @param buttonCalculate the Button value to be stored in variable
	 *                        buttonCalculate
	 */
	public void setButtonCalculate(Button buttonCalculate) {
		this.buttonCalculate = buttonCalculate;
	}

	/**
	 * Return the value stored in field labelResult.
	 * 
	 * @return results the Label value stored in variable labelResult
	 */
	public Label getLabelResult() {
		return labelResult;
	}

	/**
	 * Set the value into field labelResult.
	 * 
	 * @param labelResult the Label value to be stored in variable labelResult
	 */
	public void setLabelResult(Label labelResult) {
		this.labelResult = labelResult;
	}

	/**
	 * Return the value stored in field comboOperations.
	 * 
	 * @return comboOperations the Combo value stored in variable comboOperations
	 */
	public Combo getComboOperations() {
		return comboOperations;
	}

	/**
	 * Set the value into field comboOperations.
	 * 
	 * @param comboOperations the Combo value to be stored in variable
	 *                        comboOperations
	 */
	public void setComboOperations(Combo comboOperations) {
		this.comboOperations = comboOperations;
	}
}
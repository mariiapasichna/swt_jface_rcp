package com.mariiapasichna.views;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * Instances of this class build the behavior of Shell class, representation of
 * the "windows" which the desktop or "window manager" is managing.
 * 
 * @author Mariia Pasichna
 *
 */
public class ShellApp {
	private CTabFolderApp folder;

	/**
	 * Constructs a new instance of this class given the display to create it on and
	 * a shell value to describe its behavior and appearance.
	 * 
	 * @param shell   parent object
	 * @param display the display to create the shell on
	 */
	public ShellApp(Shell shell, Display display) {
		shell.setText("SWT Calculator");
		shell.setSize(350, 370);
		shell.setLayout(new FillLayout());
		Rectangle screenSize = display.getPrimaryMonitor().getBounds();
		shell.setLocation((screenSize.width - shell.getBounds().width) / 2,
				(screenSize.height - shell.getBounds().height) / 2);
		folder = new CTabFolderApp(shell, display);
	}

	/**
	 * Return the value stored in field folder.
	 * 
	 * @return folder the CTabFolderApp value stored in variable folder
	 */
	public CTabFolderApp getFolder() {
		return folder;
	}

	/**
	 * Set the value into field folder.
	 * 
	 * @param folder the CTabFolderApp value to be stored in variable folder
	 */
	public void setFolder(CTabFolderApp folder) {
		this.folder = folder;
	}
}
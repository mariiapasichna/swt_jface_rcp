package com.mariiapasichna.strategies;

import com.mariiapasichna.models.Result;

/**
 * The {@code Strategy} enum that implements the strategy pattern using
 * constants
 * 
 * @author Mariia Pasichna
 *
 */
public enum Strategy {
	ADDITION {
		@Override
		public Result calculate(double a, double b) {
			return new Result(a + b);
		}
	},

	DIVISION {
		@Override
		public Result calculate(double a, double b) {
			double result = 0.0;
			try {
				result = a / b;
			} catch (ArithmeticException e) {
				return new Result("Invalid operation");
			}
			return new Result(result);
		}
	},

	MULTIPLICATION {
		@Override
		public Result calculate(double a, double b) {
			return new Result(a * b);
		}
	},

	SUBTRACTION {
		@Override
		public Result calculate(double a, double b) {
			return new Result(a - b);
		}
	};

	/**
	 * Execute operation according to strategy
	 * 
	 * @param a expression operand
	 * @param b expression operand
	 * @return the object of type Result
	 * @throws ArithmeticException when divided by 0
	 */
	public abstract Result calculate(double a, double b);
}
package com.mariiapasichna.models;

import com.mariiapasichna.strategies.Strategy;

/**
 * The {@code Calculator} class provides methods to set and execute strategy. An
 * object of type {@code Calculator} contains the single field type
 * {@code Strategy}.
 * 
 * @author Mariia Pasichna
 */
public class Calculator {
	private Strategy strategy;

	/**
	 * Create an {@code Calculator} object holding the value of the specified
	 * {@code Strategy} with default value Strategy.ADDITION.
	 */
	public Calculator() {
		this.strategy = Strategy.ADDITION;
	}

	/**
	 * Set the value into field strategy.
	 * 
	 * @param operation String representation of operation
	 * @throws IllegalArgumentException if operation has unexpected value
	 */
	public void setStrategy(String operation) {
		switch (operation) {
		case "+":
			this.strategy = Strategy.ADDITION;
			break;
		case "-":
			this.strategy = Strategy.SUBTRACTION;
			break;
		case "*":
			this.strategy = Strategy.MULTIPLICATION;
			break;
		case "/":
			this.strategy = Strategy.DIVISION;
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + operation);
		}
	}

	/**
	 * Execute operation according to strategy
	 * 
	 * @param a expression operand
	 * @param b expression operand
	 * @return the object of type Result
	 */
	public Result executeStrategy(double a, double b) {
		return strategy.calculate(a, b);
	}
}
package com.mariiapasichna.controllers;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;

import com.mariiapasichna.models.Calculator;
import com.mariiapasichna.models.Result;
import com.mariiapasichna.utils.Validator;
import com.mariiapasichna.views.ShellApp;

/**
 * The {@code CalculatorController} class responsible to store data in Result
 * object and update view ShellApp accordingly. CalculatorController acts on
 * both model and view. It controls the data flow into model object and updates
 * the view whenever data changes. It keeps view and model separate.
 * 
 * @author Mariia Pasichna
 */
public class CalculatorController {
	private ShellApp shellApp;
	private Calculator calculator;

	/**
	 * Create an {@code CalculatorController} object holding the values of the
	 * specified {@code ShellApp} and {@code Calculator}.
	 * 
	 * @param calculator the value of type Calculator to be stored in variable
	 *                   calculator
	 * @param shellApp   the value of type ShellApp to be stored in variable
	 *                   shellApp
	 */
	public CalculatorController(Calculator calculator, ShellApp shellApp) {
		this.shellApp = shellApp;
		this.calculator = calculator;
		addViewListeners();
	}

	private void addViewListeners() {
		addTextField1Listener();
		addTextField2Listener();
		addComboBoxListener();
		addButtonListener();
		addCheckBoxListener();
	}

	private void addTextField1Listener() {
		Text textField = shellApp.getFolder().getPanelCalculator().getTextFieldInput1();
		textField.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				validate(textField);
			}
		});
	}

	private void addTextField2Listener() {
		Text textField = shellApp.getFolder().getPanelCalculator().getTextFieldInput2();
		textField.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				validate(textField);
			}
		});
	}

	private void addComboBoxListener() {
		Combo comboOperations = shellApp.getFolder().getPanelCalculator().getComboOperations();
		comboOperations.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				calculator.setStrategy(comboOperations.getItem(comboOperations.getSelectionIndex()));
			}
		});
	}

	private void addButtonListener() {
		Button buttonCalculate = shellApp.getFolder().getPanelCalculator().getButtonCalculate();
		buttonCalculate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				calculate();
			}
		});
	}

	private void addCheckBoxListener() {
		Button checkBox = shellApp.getFolder().getPanelCalculator().getCheckBox();
		Button buttonCalculate = shellApp.getFolder().getPanelCalculator().getButtonCalculate();
		checkBox.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (checkBox.getSelection()) {
					buttonCalculate.setEnabled(false);
				} else {
					buttonCalculate.setEnabled(true);
				}
			}
		});
	}

	private void calculate() {
		Result result = calculator.executeStrategy(
				Double.valueOf(shellApp.getFolder().getPanelCalculator().getTextFieldInput1().getText()),
				Double.valueOf(shellApp.getFolder().getPanelCalculator().getTextFieldInput2().getText()));
		String output;
		if (result.getMessage() != null) {
			output = result.getMessage();
		} else {
			output = "" + result.getResult();
		}
		shellApp.getFolder().getPanelCalculator().getTextFieldResult().setText(output);
		shellApp.getFolder().getPanelHistory().getResults().add(output, 0);
	}

	private void validate(Text textField) {
		if (shellApp.getFolder().getPanelCalculator().getCheckBox().getSelection()) {
			if (Validator.validateTextField(textField.getText())) {
				calculate();
			} else {
				textField.setText("0.0");
				calculate();
			}
		} else if (!Validator.validateTextField(textField.getText())) {
			textField.setText("0.0");
		}
	}
}
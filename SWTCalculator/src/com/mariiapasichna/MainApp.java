package com.mariiapasichna;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.mariiapasichna.controllers.CalculatorController;
import com.mariiapasichna.models.Calculator;
import com.mariiapasichna.views.ShellApp;

public class MainApp {

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		new CalculatorController(new Calculator(), new ShellApp(shell, display));
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}
}
package com.mariiapasichna.model;

/**
 * The object {@code User} class acting as a model, wraps a value of the
 * primitive type {@code long} and {@code int}, object type {@code String} in an
 * object. An object of type {@code User} contains three fields type
 * {@code long}, {@code int} and {@code String}.
 * 
 * @author Mariia Pasichna
 *
 */
public class User {
	private long id;
	private String name;
	private int group;

	/**
	 * Create an {@code User} object with default value of fields.
	 */
	public User() {
	}

	/**
	 * Return the value stored in field name.
	 * 
	 * @return name the String value stored in variable name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the value into field name.
	 * 
	 * @param name the String value to be stored in variable name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Return the value stored in field group.
	 * 
	 * @return group the int value stored in variable group
	 */
	public int getGroup() {
		return group;
	}

	/**
	 * Set the value into field group.
	 * 
	 * @param group the int value to be stored in variable group
	 */
	public void setGroup(int group) {
		this.group = group;
	}

	/**
	 * Set the value into field id.
	 * 
	 * @param id the long value to be stored in variable id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Return the value stored in field id.
	 * 
	 * @return id the long value stored in variable id
	 */
	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + group;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (group != other.group)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
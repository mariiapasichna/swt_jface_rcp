package com.mariiapasichna.repository;

import java.util.List;
import java.util.Optional;

import com.mariiapasichna.model.User;

public interface Repository {
	User save(User user);

	Optional<User> findById(long id);

	void delete(User user);

	List<User> findAll();
}
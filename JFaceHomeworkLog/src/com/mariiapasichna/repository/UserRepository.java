package com.mariiapasichna.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.mariiapasichna.model.User;


/**
 * The {@code UserRepository} class implements interface {@code Repository} and
 * provide repository layer to hold data.
 * 
 * @author Mariia Pasichna
 *
 */
public class UserRepository implements Repository {
	private static UserRepository instance;
	private Set<User> users;

	private UserRepository() {
		this.users = new HashSet<User>();
	}

	/**
	 * Thread save method, return the single instance of {@code UserRepository}
	 * class using Singleton pattern.
	 * 
	 * @return instance an instance of {@code UserRepository} class.
	 */
	public static synchronized UserRepository getInstance() {
		if (instance == null) {
			instance = new UserRepository();
		}
		return instance;
	}

	/**
	 * Save instance of {@code User} class into storage.
	 * 
	 * @param user instance of {@code User} class to be saved in storage.
	 * 
	 * @return user an instance of {@code User} class that was saved.
	 */
	public User save(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Find instance of {@code User} class in storage.
	 * 
	 * @param id index of the instance to return.
	 * 
	 * @return optional the Optional object contains the instance with the specified
	 *         id in this storage or null if it's absent.
	 */
	public Optional<User> findById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Delete instance of {@code User} class from storage.
	 * 
	 * @param user instance of {@code User} class to be deleted from storage.
	 */
	public void delete(User user) {
		// TODO Auto-generated method stub
	}

	/**
	 * Find all instances of {@code User} class from storage.
	 * 
	 * @return list the whole list of {@code User} class instances from storage.
	 */
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
}
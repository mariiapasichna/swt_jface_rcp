package com.mariiapasichna.service;

import java.util.ArrayList;
import java.util.List;

import com.mariiapasichna.exception.EntityNotFoundException;
import com.mariiapasichna.exception.NullEntityReferenceException;
import com.mariiapasichna.model.User;
import com.mariiapasichna.repository.UserRepository;

/**
 * The {@code UserServiceImpl} class implements interface {@code UserService}
 * and provide service layer representation CRUD operations.
 * 
 * @author Mariia Pasichna
 *
 */
public class UserServiceImpl implements UserService {
	private UserRepository userRepository;

	/**
	 * Constructs an instance of {@code UserServiceImpl} class and initializes the
	 * storage instance.
	 */
	public UserServiceImpl() {
		this.userRepository = UserRepository.getInstance();
	}

	/**
	 * Create an instance of {@code User} class and save it into storage.
	 * 
	 * @param user instance of {@code User} class to be created.
	 * 
	 * @return user an instance of {@code User} class that was saved.
	 * 
	 * @throws NullEntityReferenceException when we pass null argument.
	 */
	@Override
	public User create(User user) {
		try {
			return userRepository.save(user);
		} catch (IllegalArgumentException e) {
			throw new NullEntityReferenceException("User cannot be 'null'");
		}
	}

	/**
	 * Read instance of {@code User} class from storage.
	 * 
	 * @param id index of the instance to return.
	 * 
	 * @return user the instance of {@code User} class with the specified id from
	 *         this storage.
	 * 
	 * @throws EntityNotFoundException when instance is absent.
	 */
	@Override
	public User readById(long id) {
		return userRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("User with id " + id + " not found"));
	}

	/**
	 * Update an instance of {@code User} class and save it into storage.
	 * 
	 * @param user instance of {@code User} class to be updated.
	 * 
	 * @return user an instance of {@code User} class that was updated.
	 * 
	 * @throws NullEntityReferenceException when we pass null argument.
	 */
	@Override
	public User update(User user) {
		if (user == null) {
			throw new NullEntityReferenceException("User can't be null");
		}
		readById(user.getId());
		return userRepository.save(user);
	}

	/**
	 * Delete instance of {@code User} class from storage.
	 * 
	 * @param id index of the instance to be deleted from storage.
	 */
	@Override
	public void delete(long id) {
		User user = readById(id);
		userRepository.delete(user);
	}
	
	/**
	 * Get all instances of {@code User} class from storage.
	 * 
	 * @return list the whole list of {@code User} class instances from storage or
	 *         empty list.
	 */
	@Override
	public List<User> getAll() {
		List<User> users = userRepository.findAll();
		return users.isEmpty() ? new ArrayList<>() : users;
	}
}
package com.mariiapasichna.service;

import java.util.List;

import com.mariiapasichna.model.User;

public interface UserService {
	User create(User user);

	User readById(long id);

	User update(User user);

	void delete(long id);

	List<User> getAll();
}
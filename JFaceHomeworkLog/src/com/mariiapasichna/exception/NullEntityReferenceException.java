package com.mariiapasichna.exception;

/**
 * {@code NullEntityReferenceException} extends RuntimeException and can be
 * thrown during the normal operation of the Java Virtual Machine. Thrown when
 * we pass a null object reference.
 *
 * <p>
 * {@code RuntimeException} and its subclasses are <em>unchecked
 * exceptions</em>. Unchecked exceptions do <em>not</em> need to be declared in
 * a method or constructor's {@code throws} clause if they can be thrown by the
 * execution of the method or constructor and propagate outside the method or
 * constructor boundary.
 * 
 * @author Mariia Pasichna
 */
public class NullEntityReferenceException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with {@code null} as its detail message.
	 */
	public NullEntityReferenceException() {
	}

	/**
	 * Constructs a new runtime exception with the specified detail message.
	 *
	 * @param message the detail message. The detail message is saved for later
	 *                retrieval by the {@link #getMessage()} method.
	 */
	public NullEntityReferenceException(String message) {
		super(message);
	}
}